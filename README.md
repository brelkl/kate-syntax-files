# Kate(part5) Syntax Files


These were modified from the original syntax files to suit my workflow.


## How to Install Install Definition Files on KDE5

Files go in: `$HOME/.local/share/org.kde.syntax-highlighting/syntax/` (this directory may need to be created).


## The Source XML Definitions

If you'd like to see the source project it can be found here:

<https://phabricator.kde.org/source/syntax-highlighting/browse/master/data/syntax/>
